import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { jwtConstantes } from './constantes';
import { JwtPayloadDto } from './dto/jwt-payload.dto';
import { Utilisateur } from '../utilisateurs/entities/utilisateur.entity';
import { UnauthorizedException } from '@nestjs/common';

export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstantes.secret,
    });
  }

  async validate(payload: JwtPayloadDto): Promise<Utilisateur> {
    const { courriel } = payload;

    const utilisateur = await Utilisateur.findOne({
      where: {
        courriel,
      },
    });

    if (!utilisateur) {
      throw new UnauthorizedException();
    }
    return utilisateur;
  }
}