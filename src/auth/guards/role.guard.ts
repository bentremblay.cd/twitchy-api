import { CanActivate, ExecutionContext, mixin, Type } from '@nestjs/common';
import Role from '../../utilisateurs/role.enum';

export const RoleGuard = (role: Role): Type<CanActivate> => {
  class RoleGuardMixin implements CanActivate {
    async canActivate(context: ExecutionContext) {
      const request = context.switchToHttp().getRequest();
      const user = request.user;

      return user.role === role;
    }
  }

  return mixin(RoleGuardMixin);
};
