import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { InfosConnexionDto } from './dto/infos-connexion.dto';
import { ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { RoleGuard } from './guards/role.guard';
import Role from '../utilisateurs/role.enum';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post()
  connexion(@Body() infosConnexionDto: InfosConnexionDto) {
    return this.authService.connexion(infosConnexionDto);
  }

  @Post('test')
  @UseGuards(JwtAuthGuard, RoleGuard(Role.Admin))
  test(@Req() req) {
    console.log(req);
  }
}
