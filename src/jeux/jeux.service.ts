import { Injectable, NotFoundException } from '@nestjs/common';
import { Jeu } from './entities/jeu.entity';
import { CreateJeuDto } from './dto/create-jeu.dto';
import { UpdateJeuDto } from './dto/update-jeu.dto';
import { CreateStreamerDto } from '../streamers/dto/create-streamer.dto';
import { UpdateStreamerDto } from '../streamers/dto/update-streamer.dto';
import { DeepPartial, Like } from 'typeorm';
import { Genre } from '../genres/entities/genre.entity';
import { Plateforme } from '../plateformes/entities/plateforme.entity';

@Injectable()
export class JeuxService {
  async create(createJeuDto: CreateJeuDto) {
    const jeu = Jeu.create(createJeuDto as DeepPartial<Jeu>);
    jeu.plateformes = await Plateforme.find();
    return jeu.save();
  }

  async findAll() {
    return Jeu.find({
      /*where: {
        nom: Like('%Zelda%'),
      },*/
      order: {
        nom: 'ASC',
      },
    });
  }

  async findOne(id: number) {
    return Jeu.findOneOrFail({
      where: {
        id: id,
      },
    });
  }

  async update(id: number, updateJeuDto: UpdateJeuDto) {
    const jeu = await Jeu.findOneOrFail({ where: { id: id } });
    Object.assign(jeu, updateJeuDto);
    return jeu.save();
  }

  async remove(id: number) {
    const jeu = await Jeu.findOneOrFail({ where: { id: id } });
    return jeu.remove();
  }
}
