import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Genre } from '../../genres/entities/genre.entity';
import { Plateforme } from '../../plateformes/entities/plateforme.entity';

@Entity()
export class Jeu extends BaseEntity {
  @ApiModelProperty({
    example: '1',
    description: "L'identifiant du jeu",
  })
  @Column()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiModelProperty({
    example: 'The Legend of Zelda',
    description: 'Le nom du jeu',
  })
  @Column()
  nom: string;

  @ApiModelProperty({
    example:
      'Set in the fantasy land of Hyrule, the plot centers on a boy named Link, the playable protagonist...',
    description: 'La description du jeu',
  })
  @Column({ type: 'text', nullable: true })
  description: string;

  @ApiModelProperty({
    example: 10245,
    description: 'Le nombre de spectateurs regardant des streams de ce jeu',
  })
  @Column({ default: 0, type: 'int' })
  spectateurs: number;

  @ManyToOne(() => Genre, (genre) => genre.jeux, {
    //eager: true,
  })
  genre: Genre;

  @ManyToMany(() => Plateforme, {
    //eager: true,
  })
  @JoinTable()
  plateformes: Plateforme[];
}
