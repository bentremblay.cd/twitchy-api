import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString, MinLength } from 'class-validator';

export class CreateJeuDto {
  @ApiProperty({
    example: 'The Legend of Zelda',
    description: 'Le nom du jeu',
  })
  @IsString({
    message: 'Le nom du jeu est de type texte',
  })
  @MinLength(3)
  nom: string;

  @ApiProperty({
    example:
      'Set in the fantasy land of Hyrule, the plot centers on a boy named Link, the playable protagonist...',
    description: 'La description du jeu',
  })
  @IsString()
  @MinLength(50)
  description: string;

  @ApiProperty({
    example: 1,
    description: "Le id d'un genre de jeu",
  })
  @IsNumber()
  @IsOptional()
  genreId?: number;
}
