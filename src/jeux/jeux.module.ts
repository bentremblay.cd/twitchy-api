import { Module } from '@nestjs/common';
import { JeuxController } from './jeux.controller';
import { JeuxService } from './jeux.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Jeu } from './entities/jeu.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Jeu])],
  controllers: [JeuxController],
  providers: [JeuxService],
})
export class JeuxModule {}
