import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Put, UseGuards
} from '@nestjs/common';
import { JeuxService } from './jeux.service';
import { CreateJeuDto } from './dto/create-jeu.dto';
import { UpdateJeuDto } from './dto/update-jeu.dto';
import {
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Jeu } from './entities/jeu.entity';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import Role from '../utilisateurs/role.enum';
import { RoleGuard } from '../auth/guards/role.guard';

@ApiTags('jeux')
@Controller('jeux')
export class JeuxController {
  constructor(private jeuxService: JeuxService) {}

  @ApiCreatedResponse({
    type: Jeu,
  })
  @Post()
  @UseGuards(JwtAuthGuard, RoleGuard(Role.Admin))
  async create(@Body() createJeuDto: CreateJeuDto) {
    return this.jeuxService.create(createJeuDto);
  }

  @ApiOkResponse({
    type: [Jeu],
  })
  @Get()
  async findAll() {
    return this.jeuxService.findAll();
  }

  @ApiOkResponse({
    type: Jeu,
  })
  @ApiNotFoundResponse({
    description: "Si le jeu n'est pas troubé",
  })
  @Get(':id')
  async findOne(@Param('id') id: string) {
    return this.jeuxService.findOne(+id).catch((erreur) => {
      throw new NotFoundException();
    });
  }

  @ApiOkResponse({
    type: Jeu,
  })
  @Patch(':id')
  @UseGuards(JwtAuthGuard, RoleGuard(Role.Admin))
  update(@Param('id') id: string, @Body() updateJeuDto: UpdateJeuDto) {
    return this.jeuxService.update(+id, updateJeuDto).catch((erreur) => {
      throw new NotFoundException();
    });
  }

  @ApiOkResponse()
  @Delete(':id')
  @UseGuards(JwtAuthGuard, RoleGuard(Role.Admin))
  remove(@Param('id') id: string) {
    return this.jeuxService.remove(+id).catch((erreur) => {
      throw new NotFoundException();
    });
  }
}
