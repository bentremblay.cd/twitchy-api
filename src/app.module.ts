import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { JeuxModule } from './jeux/jeux.module';
import { StreamersModule } from './streamers/streamers.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { UtilisateursModule } from './utilisateurs/utilisateurs.module';
import { GenresModule } from './genres/genres.module';
import { PlateformesModule } from './plateformes/plateformes.module';

@Module({
  imports: [
    JeuxModule,
    StreamersModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'gestion_bd',
      password: 'bd3N3_1234!',
      database: 'twitchy',
      autoLoadEntities: true,
      synchronize: true,
      logging: true,
    }),
    AuthModule,
    UtilisateursModule,
    GenresModule,
    PlateformesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
