import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { IsString } from 'class-validator';
import { Jeu } from '../../jeux/entities/jeu.entity';

@Entity()
export class Genre extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @IsString()
  @Column()
  nom: string;

  @OneToMany(() => Jeu, (jeu) => jeu.genre)
  jeux: Jeu[];
}
