import { BaseEntity, Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Jeu } from '../../jeux/entities/jeu.entity';

@Entity()
export class Plateforme extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nom: string;

  @ManyToMany(() => Jeu)
  jeux: Jeu[];
}
