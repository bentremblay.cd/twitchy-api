import { Module } from '@nestjs/common';
import { PlateformesService } from './plateformes.service';
import { PlateformesController } from './plateformes.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Jeu } from '../jeux/entities/jeu.entity';
import { Plateforme } from './entities/plateforme.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Plateforme])],
  controllers: [PlateformesController],
  providers: [PlateformesService]
})
export class PlateformesModule {}
