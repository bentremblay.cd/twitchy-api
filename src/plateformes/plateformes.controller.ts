import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { PlateformesService } from './plateformes.service';
import { CreatePlateformeDto } from './dto/create-plateforme.dto';
import { UpdatePlateformeDto } from './dto/update-plateforme.dto';

@Controller('plateformes')
export class PlateformesController {
  constructor(private readonly plateformesService: PlateformesService) {}

  @Post()
  create(@Body() createPlateformeDto: CreatePlateformeDto) {
    return this.plateformesService.create(createPlateformeDto);
  }

  @Get()
  findAll() {
    return this.plateformesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.plateformesService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePlateformeDto: UpdatePlateformeDto) {
    return this.plateformesService.update(+id, updatePlateformeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.plateformesService.remove(+id);
  }
}
