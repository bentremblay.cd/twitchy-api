import { Test, TestingModule } from '@nestjs/testing';
import { PlateformesService } from './plateformes.service';

describe('PlateformesService', () => {
  let service: PlateformesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PlateformesService],
    }).compile();

    service = module.get<PlateformesService>(PlateformesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
