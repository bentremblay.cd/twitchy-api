import { Test, TestingModule } from '@nestjs/testing';
import { PlateformesController } from './plateformes.controller';
import { PlateformesService } from './plateformes.service';

describe('PlateformesController', () => {
  let controller: PlateformesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PlateformesController],
      providers: [PlateformesService],
    }).compile();

    controller = module.get<PlateformesController>(PlateformesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
