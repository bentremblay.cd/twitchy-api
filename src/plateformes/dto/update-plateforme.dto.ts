import { PartialType } from '@nestjs/swagger';
import { CreatePlateformeDto } from './create-plateforme.dto';

export class UpdatePlateformeDto extends PartialType(CreatePlateformeDto) {}
