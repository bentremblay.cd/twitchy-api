import { Injectable } from '@nestjs/common';
import { CreatePlateformeDto } from './dto/create-plateforme.dto';
import { UpdatePlateformeDto } from './dto/update-plateforme.dto';

@Injectable()
export class PlateformesService {
  create(createPlateformeDto: CreatePlateformeDto) {
    return 'This action adds a new plateforme';
  }

  findAll() {
    return `This action returns all plateformes`;
  }

  findOne(id: number) {
    return `This action returns a #${id} plateforme`;
  }

  update(id: number, updatePlateformeDto: UpdatePlateformeDto) {
    return `This action updates a #${id} plateforme`;
  }

  remove(id: number) {
    return `This action removes a #${id} plateforme`;
  }
}
