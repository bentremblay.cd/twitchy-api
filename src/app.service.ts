import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(nom: string): string {
    return `Hello ${nom}`;
  }
}
