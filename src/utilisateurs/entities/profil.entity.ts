import { BaseEntity, Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Utilisateur } from './utilisateur.entity';

@Entity()
export class Profil extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  compteTwitch: string;

  @OneToOne(() => Utilisateur)
  utilisateur: Utilisateur;
}