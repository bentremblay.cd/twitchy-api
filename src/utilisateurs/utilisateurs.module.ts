import { Module } from '@nestjs/common';
import { UtilisateursController } from './utilisateurs.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Utilisateur } from './entities/utilisateur.entity';
import { UtilisateursService } from './utilisateurs.service';
import { Profil } from './entities/profil.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Utilisateur, Profil])],
  controllers: [UtilisateursController],
  providers: [UtilisateursService],
})
export class UtilisateursModule {}
